const MAILCHIMPCONFIG = require('./mailchimp.js')
require('dotenv').config( { path: '../.env' } )

async function ping() {
    const response = await MAILCHIMPCONFIG.mailchimp.lists.getAllLists();
    console.log(response);
}

//ping();