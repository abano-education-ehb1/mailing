const xml2js = require('xml2js')
const MAILCHIMPCONFIG = require('./mailchimp.js')

let subscribingUser

function parsing(xmlBody){
    const parser = new xml2js.Parser()
    parser.parseString(xmlBody, (err, result) => {
        if(err){
            console.log('Error with body parser')
            throw err
        }

        console.log(`Action: ${result.data.action[0]}`)
        console.log(`UUID: ${result.data.identifiers[0].uuid[0]}`)

        if(result.data.action[0] === 'create' || result.data.action[0] === 'update'){
            subscribingUser = {
                firstName: result.data.properties[0].firstname[0],
                lastName: result.data.properties[0].lastname[0],
                email: result.data.properties[0].email[0],
                phone: result.data.properties[0].phone[0],
                company: result.data.properties[0].company[0],
                address: {
                    addr1: result.data.properties[0].address[0].street[0] + ' ' + result.data.properties[0].address[0].housenumber[0],
                    city: result.data.properties[0].address[0].city[0],
                    country: result.data.properties[0].address[0].country[0]
                },
                uuid: result.data.identifiers[0].uuid[0],
                action: result.data.action[0]
            };
            MAILCHIMPCONFIG.createContact(subscribingUser)
        }   
    })
}

//parsing(xmlString)

module.exports = { parsing }