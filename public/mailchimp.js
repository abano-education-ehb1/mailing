//config
const mailchimp = require('@mailchimp/mailchimp_marketing')
require('dotenv').config( { path: '../.env' } )
const NOTIFYCONFIG = require('./rabbitmq/notify')
const SENDERCONFIG = require('./rabbitmq/sender')
const md5 = require('md5')
mailchimp.setConfig({
    apiKey: process.env.API_KEY,
    server: process.env.API_DC
})
async function getList() {
    const response = await mailchimp.lists.getAllLists();
    console.log(response);
}
//getList()

const subscribingUser = {
    firstName: 'Miyuki',
    lastName: 'Kazuya',
    email: 'sniper@gmail.com',
    phone: '+32456789145',
    company: 'Seido',
    address: {
        addr1: 'Test street 12',
        city: 'Brussels',
        country: 'Belgium',
        state: 'none',
        zip: 'none',
    }
};

async function createContact(properties){
    try {
        const response = await mailchimp.lists.setListMember('4effd2adcc', properties.email, {
            email_address: properties.email,
            status: "subscribed",
            merge_fields: {
              FNAME: properties.firstName,
              LNAME: properties.lastName,
              PHONE: properties.phone,
              //COMPANY: properties.company,
              ADDRESS: {
                  addr1: properties.address.addr1,
                  city: properties.address.city,
                  state: properties.address.country,
                  zip: 'none',
                  country: properties.address.country
              }
            }
        }).catch(err => {
              console.log(err)
        })
    
        if(response.id){
            console.log(
                `Successfully added contact as an audience member. 
                The contact's id is ${response.id}.`
            );
            NOTIFYCONFIG.notifyUUID(properties, response.id, 'user')
            return response.id
        }
    } catch (error) {
        let error_object = {
            message: error.message + ' (location: mailchimp.js createContact method)',
            level: 'error'
        }
        console.log(error_object)
        SENDERCONFIG.sendError(error_object)
    }
    
}

async function deleteContact(email, uuid, entity){
    const subscriberHash = md5(email.toLowerCase())
    try {
        const response = await mailchimp.lists.deleteListMember(
            "4effd2adcc",
            subscriberHash
        );
        console.log('Contact successfully deleted');
        let properties = {
            action: 'delete',
            uuid: uuid
        }
        NOTIFYCONFIG.notifyUUID(properties, subscriberHash, entity)
        console.log('Response: ' + response)
    } catch (error) {
        let error_object = {
            message: error.message + ' (location: mailchimp.js deleteContact method)',
            level: 'error'
        }
        console.log(error_object)
        SENDERCONFIG.sendError(error_object)
    }
}

async function createCompany(properties){
    try {
        const response = await mailchimp.lists.setListMember('4effd2adcc', properties.email, {
            email_address: properties.email,
            status: "subscribed",
            merge_fields: {
              FNAME: properties.firstName,
              PHONE: properties.phone,
              //COMPANY: properties.company,
              ADDRESS: {
                  addr1: properties.address.addr1,
                  city: properties.address.city,
                  state: properties.address.country,
                  zip: 'none',
                  country: properties.address.country
              }
            }
        }).catch(err => {
              console.log(err)
        })
    
        if(response.id){
            console.log(
                `Successfully added company as an audience member. 
                The company's id is ${response.id}.`
            );
            NOTIFYCONFIG.notifyUUID(properties, response.id, 'company')
        }
    } catch (error) {
        let error_object = {
            message: error.message + ' (location: mailchimp.js createCompany method)',
            level: 'error'
        }
        console.log(error_object)
        SENDERCONFIG.sendError(error_object)
    }
    
}

async function updateSession(email, session, action){
    const subscriberHash = md5(email.toLowerCase())
    let object
    if(action === 'create'){
        object = {tags: [{name: session, status: "active"}] }
    }else{
        object = {tags: [{name: session, status: "inactive"}] }
    }
    try {
        const response = await mailchimp.lists.updateListMemberTags(
            '4effd2adcc', 
            subscriberHash, 
            object)  
        console.log(
            `The return type for this endpoint is null, so this should be true: ${response === null}`
        );    
    } catch (error) {
        let error_object = {
            message: error.message + ' (location: mailchimp.js updateSession method)',
            level: 'error'
        }
        SENDERCONFIG.sendError(error_object)
    }
}
//createContact(subscribingUser);
module.exports = { mailchimp, getList, createContact, deleteContact, updateSession, createCompany }