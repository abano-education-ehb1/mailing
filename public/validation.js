const libxmljs = require("libxmljs")
const xml2js = require('xml2js')
const fs = require('fs').promises
const MAILCHIMPCONFIG = require('./mailchimp.js')
const EMAILSENDERCONFIG = require('./email_sender')
const SENDERCONFIG = require('./rabbitmq/sender')

let xml_session = `
<session>
<source>frontend</source>
<action>create/update/delete</action>
<source-id>5</source-id>
<uuid>5d0239a4-d14e-11ec-9d64-0242ac120002</uuid>
<properties>
  <name>Test Session</name>
  <beginDateTime>2022-04-12 16:00:00</beginDateTime>
  <endDateTime>2022-04-12 18:00:00</endDateTime>
  <speaker>Bob De bouwer</speaker>
  <location>A1</location>
  <description>Event description.</description>
</properties>
</session>
`
let xml_string = `
    <user>
        <source>frontend</source>
        <source-id>string</source-id>
        <uuid>string</uuid>
        <action>create</action>
        <properties>
            <firstname>Eijun</firstname>
            <lastname>Sawamura</lastname>
            <email>deletetete@gmail.com</email>
            <address>
                <street>streetvvvivi</street>
                <housenumber>463</housenumber>
                <postalcode>1190</postalcode>
                <city>Tokyo</city>
                <country>Japan</country>
            </address>
            <phone>04567823</phone>
            <company>
                <source-id>string</source-id>
                <uuid>string</uuid>
            </company>
        </properties>
    </user>
`

let xml_company = `<company>
  <source>string</source>
  <source-id>string</source-id>
  <uuid>string</uuid>
  <action>create</action>
  <properties>
    <name>string</name>
    <email>company@gmail.com</email>
    <address>
      <street>string</street>
      <housenumber>4635</housenumber>
      <postalcode>1802</postalcode>
      <city>string</city>
      <country>string</country>
    </address>
    <phone>string</phone>
    <taxId>string</taxId>
  </properties>
</company>`
let xml_user_session = `<user-session>
<source>frontend</source>
<sessionId>sessionID</sessionId>
<sessionName>sessionNAME</sessionName>
<userId>string</userId>
<action>create</action>
<email>pipeline@gmail.com</email>
</user-session>
`

let xml_invoice = `<?xml version="1.0" encoding="UTF-8"?>
<send-invoices>
   <source>facturatie</source>
   <links>
      <link>http://10.3.56.3:9100/invoice/pdf/ae3923beadb2ef06546b0de18c953ed685e82c24</link>
   </links>
   <emails>
      <email>totoji3438@doerma.com</email>
   </emails>
</send-invoices>
`
async function getXsd(xml_body){
    let pathXsdUser = './schemas/user.xsd'
    let pathXsdSession = './schemas/session.xsd'
    let pathXsdUserSession = './schemas/user-session.xsd'
    let pathXsdInvoice = './schemas/invoice.xsd'
    try{
        const data = await fs.readFile(pathXsdUser)
        const user_xsd = Buffer.from(data).toString()
        const data2 = await fs.readFile(pathXsdSession)
        const session_xsd = Buffer.from(data2).toString()
        const data3 = await fs.readFile(pathXsdUserSession)
        const user_session_xsd = Buffer.from(data3).toString()
        const data4 = await fs.readFile(pathXsdInvoice)
        const invoice_xsd = Buffer.from(data4).toString()
        checkXsd(user_xsd, session_xsd, user_session_xsd, invoice_xsd, xml_body)
    }catch(error){
        error_object = {
            message: error.message + ' (location: validation.js getXsd method)',
            level: 'error'
        }
        console.log(error_object)
        SENDERCONFIG.sendError(error_object)
    }  
}
async function getXsdV2(xml_body){
    try{
        const parser = new xml2js.Parser()
        parser.parseString(xml_body, (error, result) => {
            if(error){
                let error_object = {
                    message: `(location: validation.js getXsdV2 method) ${error.message}`,
                    level: 'error'
                }
                console.log(error_object.message)
                SENDERCONFIG.sendError(error_object)
            }
            let key = Object.keys(result)[0]
            if(key === 'user'){
                sendUserXml(xml_body)
            }else if(key === 'user-session'){
                sendSessionUserXml(xml_body)
            }else if(key === 'send-invoices'){
                sendInvoiceXml(xml_body)
            }else if(key === 'company'){
                sendCompanyXml(xml_body)
            }else{
                console.log('geen XML')
            }
        })
    }catch(error){
        error_object = {
            message: ' (location: validation.js getXsdV2 method)' + error.message,
            level: 'error'
        }
        console.log(error_object)
        SENDERCONFIG.sendError(error_object)
    }  
}
/*function checkXsd(user_xsd, session_xsd, user_session_xsd, invoice_xsd, xml){
    try{
        let xsdDOC = libxmljs.parseXml(user_xsd)
        let parsedXml = libxmljs.parseXml(xml)
        let check = parsedXml.validate(xsdDOC)
        console.log('User xml: ' + check)
        if(check){
            sendUserXml(xml)
        }else{
            xsdDOC = libxmljs.parseXml(user_session_xsd)
            parsedXml = libxmljs.parseXml(xml)
            check = parsedXml.validate(xsdDOC)
            console.log('User Session xml: ' + check)
            if(check){   
                sendSessionUserXml(xml)
            } else{
                xsdDOC = libxmljs.parseXml(invoice_xsd)
                parsedXml = libxmljs.parseXml(xml)
                check = parsedXml.validate(xsdDOC)
                console.log('Invoice xml: ' + check)
                if(check){   
                    sendInvoiceXml(xml)
                }
            }
        }  
    }catch(error){
        let error_object = {
            message: `(location: validation.js checkXsd method) ${error.message}`,
            level: 'error'
        }
        console.log(error_object.message)
        SENDERCONFIG.sendError(error_object)
    }
}*/
async function sendCompanyXml(xml){
    const parser = new xml2js.Parser()
    try{
        parser.parseString(xml, (error, result) => {
            if(error){
                console.log('Error with body parser')
                let error_object = {
                    message: `(location: validation.js sendUserXml method) ${error.message}`,
                    level: 'error'
                }
                console.log(error_object.message)
                SENDERCONFIG.sendError(error_object)
            }
            console.log(result)
            let action = result.company.action[0]
            let subscribingUser
            console.log('Action: ' + action)
           
            if(action === 'create' || action === 'update'){ 
                let properties = result.company.properties[0]
                subscribingUser = {
                    firstName: properties.name[0],
                    email: properties.email[0],
                    phone: properties.phone[0],
                    //company: properties.company[0],
                    address: {
                        addr1: properties.address[0].street[0] + ' ' + properties.address[0].housenumber[0],
                        city: properties.address[0].city[0],
                        country: properties.address[0].country[0]
                    },
                    uuid: result.company.uuid[0],
                    action: action
                }
                console.log(subscribingUser)
                MAILCHIMPCONFIG.createCompany(subscribingUser)
            }else if(action === 'delete'){
                MAILCHIMPCONFIG.deleteContact(result.company.properties[0].email[0], result.company.uuid[0], 'company')
            }   
        })
    }catch(error){
        let error_object = {
            message: `(location: validation.js sendUserXml method) ${error.message}`,
            level: 'error'
        }
        console.log(error_object.message)
        SENDERCONFIG.sendError(error_object)
    }
}
async function sendUserXml(xml){
    const parser = new xml2js.Parser()
    try{
        parser.parseString(xml, (error, result) => {
            if(error){
                console.log('Error with body parser')
                let error_object = {
                    message: `(location: validation.js sendUserXml method) ${error.message}`,
                    level: 'error'
                }
                console.log(error_object.message)
                SENDERCONFIG.sendError(error_object)
            }
            console.log(result)
            let action = result.user.action[0]
            let subscribingUser
            console.log(action)
            console.log(result.user.properties[0].firstname[0])
            if(action === 'create' || action === 'update'){ 
                let properties = result.user.properties[0]
                subscribingUser = {
                    firstName: properties.firstname[0],
                    lastName: properties.lastname[0],
                    email: properties.email[0],
                    phone: properties.phone[0],
                    //company: properties.company[0],
                    address: {
                        addr1: properties.address[0].street[0] + ' ' + properties.address[0].housenumber[0],
                        city: properties.address[0].city[0],
                        country: properties.address[0].country[0]
                    },
                    uuid: result.user.uuid[0],
                    action: action
                }
                //console.log(subscribingUser)
                MAILCHIMPCONFIG.createContact(subscribingUser)
            }else if(action === 'delete'){
                MAILCHIMPCONFIG.deleteContact(result.user.properties[0].email[0], result.user.uuid[0], 'user')
            }   
        })
    }catch(error){
        let error_object = {
            message: `(location: validation.js sendUserXml method) ${error.message}`,
            level: 'error'
        }
        console.log(error_object.message)
        SENDERCONFIG.sendError(error_object)
    }
      
}

async function sendSessionUserXml(xml){
    console.log(xml)
    const parser = new xml2js.Parser()
    try{
        parser.parseString(xml, (error, result) => {
            if(error){
                console.log(error.message)
                let error_object = {
                    message: `(location: validation.js sendSessionUsereXml method) ${error.message}`,
                    level: 'error'
                }
                console.log(error_object.message)
                SENDERCONFIG.sendError(error_object)
            }
            let action = result['user-session'].action[0]
            let email = result['user-session'].email[0]
            let session = result['user-session'].sessionName[0]
            console.log(action)
            console.log(email)
            console.log(session)
            MAILCHIMPCONFIG.updateSession(email, session, action)
        })
    }catch(error){
        let error_object = {
            message: `(location: validation.js sendSessionUserXml method) ${error.message}`,
            level: 'error'
        }
        console.log(error_object.message)
        SENDERCONFIG.sendError(error_object)
    }
}

async function sendInvoiceXml(xml){
    console.log('Entered sendInvoiceXml method')
    const parser = new xml2js.Parser()
    try {
        parser.parseString(xml, (error, result) => {
            if(error){
                console.log(error.message)
                let error_object = {
                    message: `(location: validation.js sendInvoiceXml method) ${error.message}`,
                    level: 'error'
                }
                console.log(error_object.message)
                SENDERCONFIG.sendError(error_object)
            }
            let arrayLinks = []
            let arrayMails = []
            
            arrayLinks = result['send-invoices'].links[0].link
            arrayMails = result['send-invoices'].emails[0].email
            console.log(arrayLinks.length)
            for (let i = 0; i < arrayLinks.length; i++) {
                let properties = {
                    path: result['send-invoices'].links[0].link[i],
                    email: result['send-invoices'].emails[0].email[i]
                }
                //console.log(properties)
                EMAILSENDERCONFIG.sendEmail(properties)
            }
            EMAILSENDERCONFIG.sendEmail(properties)
            //console.log(properties)
        })

    } catch (error) {
        console.log(error.message)
    }
    
}
//getXsd(xml_invoice)
//sendUserXml(xml_string)
//sendInvoiceXml(xml_invoice)
//getXsdV2(xml_company)
//sendSessionUserXml(xml_user_session)
//sendCompanyXml(xml_company)
module.exports = { getXsd, sendSessionUserXml, sendUserXml, sendInvoiceXml, getXsdV2 }