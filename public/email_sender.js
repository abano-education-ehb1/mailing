const nodemailer = require('nodemailer')
const { google } = require('googleapis')
require('dotenv').config( { path: '../.env' } )

const oAuth2Client = new google.auth.OAuth2(process.env.GOOGLE_GMAIL_CLIENT_ID, process.env.GOOGLE_GMAIL_CLIENT_SECRET, process.env.GOOGLE_GMAIL_REDIRECT_URI)
oAuth2Client.setCredentials({refresh_token: process.env.GOOGLE_GMAIL_REFRESH_TOKEN})

async function sendEmail(properties){
    console.log('Entered sendEMAIL method')
    try {
        const accessToken = oAuth2Client.getAccessToken()
        const transport = nodemailer.createTransport({
            service: 'gmail',
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            auth: {
                type: 'OAuth2',
                user: 'integrationproject22@gmail.com',
                clientId: process.env.GOOGLE_GMAIL_CLIENT_ID,
                clientSecret: process.env.GOOGLE_GMAIL_CLIENT_SECRET,
                refreshToken: process.env.GOOGLE_GMAIL_REFRESH_TOKEN,
                accessToken: accessToken
            }
        })
        const mailOptions = {
            from: process.env.EMAIL,
            to: properties.email,
            subject: 'Invoice from event',
            text: 'Attached you will find your invoice',
            attachments: [{
                filename: 'invoice.pdf',  
                path: properties.path,                                       
                contentType: 'application/pdf'
            }]
        }
        const result = await transport.sendMail(mailOptions)
        console.log(result)
    } catch (error) {
        console.log(error)
    }
}

//sendEmail()

module.exports = { sendEmail }