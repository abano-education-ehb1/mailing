const amqp = require('amqplib/callback_api')
require('dotenv').config( { path: '../../.env' } )

const opt = { 
    credentials: amqp.credentials.plain(
        process.env.RABBIT_USER, 
        process.env.RABBIT_PASSWORD
    ) 
}

const queue = 'errors'

const xmlString =`<data>
    <source>frontend</source>
    <entity>user</entity>
    <action>update</action>
    <identifiers>
        <uuid>123e4567-e89b-12d3-a456-426614174000</uuid>
        <frontend>14</frontend>
    </identifiers>
    <properties>
        <firstname>Sawamura</firstname>
        <lastname>DAce</lastname>
        <email>SawamuraD1@ehb.be</email>
        <password>**</password>
        <address>
            <street>JSKDJSLQKJS</street>
            <housenumber>50</housenumber>
            <city>Halle</city>
            <country>Belgium</country>
        </address>
        <phone>+3204775082205</phone>
        <company>Gamma</company>
    </properties>
</data>`
function sendHeartbeat(xml){
    amqp.connect(`amqp://${process.env.VM}`, opt, (error, connection) => {
        if(error){
            console.log(error)
            throw error
        }
        
        connection.createChannel((channelError, channel) => {
            if(channelError){
                console.log(channelError)
                throw channelError
            }
            //let nospace = body.message.replace(/(\r\n|\n|\r)/gm, "")
            channel.sendToQueue('monitoring', Buffer.from(xml))
            //channel.assertExchange('uuid_manager_exchange', 'fanout', {
            //    durable: false
            //})
            //channel.publish('uuid_manager_exchange', '', Buffer.from(xmlString))
            //    console.log('Message sent')
            //console.log('heartbeat sent!')
        })

        setTimeout(function() {
            connection.close();
        }, 1000)
    })
}
function sendError(body){
    amqp.connect(`amqp://${process.env.VM}`, opt, (error, connection) => {
        if(error){
            console.log(error)
            throw error
        }
        
        connection.createChannel((channelError, channel) => {
            if(channelError){
                console.log(channelError)
                throw channelError
            }
            let nospace = body.message.replace(/(\r\n|\n|\r)/gm, "")
            let xml_error = `<error>
                <source>mailing</source>
                <date>12312</date>
                <level>${body.level}</level>
                <message>${nospace}</message>
            </error>`
            channel.sendToQueue(queue, Buffer.from(xml_error))
            //channel.assertExchange('uuid_manager_exchange', 'fanout', {
            //    durable: false
            //})
            //channel.publish('uuid_manager_exchange', '', Buffer.from(xmlString))
            console.log('Error sent to monitoring')
        })

        setTimeout(function() {
            connection.close();
        }, 1000)
    })
}

function printError(error){
    let xml_error = `
            <error>
                <source>mailing</source>
                <date>${Date.now()}</date>
                <level>${error.level}</level>
                <message>${error.message}</message>
            </error>
            `
    console.log(xml_error)        
}
module.exports = { sendError, printError, sendHeartbeat }