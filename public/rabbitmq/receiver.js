const amqp = require('amqplib/callback_api')
require('dotenv').config( { path: '../../.env' } )
const VALIDATIONCONFIG = require('../validation')
const SENDERCONFIG = require('./sender')

const opt = { 
    credentials: amqp.credentials.plain(
        process.env.RABBIT_USER,
        process.env.RABBIT_PASSWORD
    ) 
}

const queue = 'mailing'
const exchange = 'uuid_manager_exchange'

function listen(){
    amqp.connect(`amqp://${process.env.VM}`, opt, (error, connection) => {
        if (error) {
            console.log(error)
            throw error;
        }

        connection.createChannel((errorChannel, channel) => {
            if (errorChannel) {
                console.log(errorChannel)
                throw errorChannel;
            }
            
            channel.assertQueue(queue, {
                durable: false
            });
            //channel.bindQueue(queue, exchange, '')
            //channel.bindQueue(queue, exchange, '')
            console.log('[*] Waiting for messages in %s. To exit press CTRL+C', queue)

            channel.consume(queue, (msg) => {
                console.log(msg.content.toString());
                //console.log(msg.content.toString())
                try {
                    VALIDATIONCONFIG.getXsdV2(msg.content.toString())
                } catch (error) {
                    let error_object = {
                        message: `(location: receiver.js listen method) ${error.message}`,
                        level: 'error'
                    }
                    console.log(error_object.message)
                    SENDERCONFIG.sendError(error_object)
                }
                
                //console.log('[*] Waiting for messages in %s. To exit press CTRL+C', queue)
                console.log('Received sending mail')
            }, { noAck: true })
        });
    });
}

let heartbeat = () => {
    const currentDate = new Date()
    const timestamp = currentDate.getTime()
    let xml = `<heartbeat>
    <source>mailing</source>
    <date>1650968324</date>
</heartbeat>`
    //console.log(xml)
    SENDERCONFIG.sendHeartbeat(xml)
}
setInterval(heartbeat, 3000)
listen()