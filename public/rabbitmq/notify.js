const amqp = require('amqplib/callback_api')
require('dotenv').config( { path: '../../.env' } )

const opt = { 
    credentials: amqp.credentials.plain(
        process.env.RABBIT_USER,
        process.env.RABBIT_PASSWORD
    ) 
}
const queue = 'uuid_manager'

function notifyUUID(properties, id, entity){
    if(properties.action === 'create'){
        properties.action = 'created'
    }else if(properties.action === 'update'){
        properties.action = 'updated'
    }else if(properties.action === 'delete'){
        properties.action = 'deleted'
    }
    console.log('Notity action: ' + properties.action)
    console.log(`Notify UUID: ${properties.uuid}`)
const xmlString =`<response>
    <source>mailing</source>
    <source-id>${id}</source-id>
    <uuid>${properties.uuid}</uuid>
    <entity>${entity}</entity>
    <action>${properties.action}</action>
</response>`
    console.log(xmlString)
    amqp.connect(`amqp://${process.env.VM}`, opt, (error, connection) => {
        if(error){
            console.log(error)
            throw error
        }

        connection.createChannel((channelError, channel) => {
            if(channelError){
                console.log(channelError)
                throw channelError
            }
            channel.assertQueue(queue, {
                durable: false
            })
            channel.sendToQueue(queue, Buffer.from(xmlString))
            console.log('Notification sent')
        })

        
    })
}

module.exports = { notifyUUID }