FROM node:14-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
WORKDIR /app/public/rabbitmq
CMD ["node", "./receiver.js"]
